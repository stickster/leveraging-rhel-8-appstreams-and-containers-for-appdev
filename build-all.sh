#!/bin/bash
set -x

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi


# Get the absolute source directory
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

ln -s $DIR /root/appstream-lab

yum install -y make
echo `date` > /root/build.log
cd /root/appstream-lab/fedmod-container/
make build >> /root/build.log 2>&1
make push  >> /root/build.log 2>&1

cd /root/appstream-lab/fedpkg-container/
make build >> /root/build.log 2>&1
make push  >> /root/build.log 2>&1

cd /root/appstream-lab/mbs-container/
make build >> /root/build.log 2>&1
make push  >> /root/build.log 2>&1

