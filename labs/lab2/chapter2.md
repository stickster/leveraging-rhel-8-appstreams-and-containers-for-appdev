[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 2: Using Python & Making Application Streams
## Introduction

In this lab we will discuss and install Python and then create two Application Streams.

This lab should be performed on `workstation.example.com` as `student` unless otherwise instructed.

Expected completion: 45-60 minutes

NOTE: much of the superfluous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## Prerequisites

This lab requires that you have completed [Lab 1](../lab1/chapter1.md).

If you have not done so (or have been unable to complete it successfully, there is an ansible playbook on `workstation.example.com` that will set the system up.

```
$ cd ~/appstream-lab/labs/lab2/support-files/playbooks
$ ansible-playbook -i hosts lab2-setup-play.yml
```
This should only take a moment to complete.

## Python in RHEL8

Python 2 has served us faithfully for many years in RHEL.
But, as everyone should be aware, its days are numbered.
Python 2 upstream support will end on January 1, 2020.

RHEL8 is a Python 3-only distribution.
Thus, RHEL8 ships without Python 2.
There's not even a binary named "python" to avoid setting a default.

If you want more details on the reasoning for the python strategy in RHEL8, there is a convenient [blog post](https://developers.redhat.com/blog/2018/11/27/what-no-python-in-rhel-8-beta/) on the subject.

However, we understand many customers still need Python 2 for their applications.
Thus, in RHEL8, Python 2 is provided as a module.
So is Python 3.

```bash
$ yum module list | grep '^python'
python27     2.7 [d]     common [d]               Python programming language, version 2.7
python36     3.6 [d][e]  common [d], build        Python programming language, version 3.6
```

Just like with the old non-modular versions of Python, you can install both Python 2 and Python 3 at the same time.

## Author Flask ModuleMD

For this lab, we will be developing a fictitious application for ACME Corp based on Python's Flask Microframework.
However, ACME Corp currently has Python 2 deployed in their production environment.
The IT leadership at ACME Corp promises that the upgrade to Python 3 will be in place by the time your project needs to be deployed.
However, as you have worked in software before, you don't trust them.
As a result, you are going to develop the application in Python 3 but you are going to make sure it is also compatible with Python 2 for when the schedule slips.

In order to accomplish this, we are going to package ("modularize") two versions of flask, one based on the Python 2 stack and one based on Python 3.
As a result, we have to create two modulemds to describe the two different streams.
The two module streams will be `flask:py27` & `flask:py36`

However, we need to get our tools first, much like we would get `mock` to build RPMs.
Many of the tools you need for building modules are built and deployed on Fedora but will not be shipping in RHEL.
As a result, like many of the build tools in the past, you should be able to find these in EPEL when EPEL8 launches.
In the meantime, we have built some containers based on Fedora that we are using to execute the tools.
We also need to do a little preliminary setup so we can use `git`.
The module build service, the application we will be building modules with, relies on git commit hashes to make sure it has the correct version of the components of the module you want to build.

```bash
$ git config --global user.email "you@example.com"
$ git config --global user.name "Your Name"
```
And `fedpkg` but for now we will use a prebuilt container that's based on Fedora.
To get it:

```bash
$ cd ~/appstream-lab/fedpkg-container
$ make pull
```

Make container wrapper available from the command line:

```bash
$ mkdir -p ~/bin
$ ln -sf $PWD/fedpkg.sh ~/bin/fedpkg
```

Next, let's create the module metadata definition (modulemd) YAML files and put them in a local `git` repository.
It is customary to put the modulemds in `git` "stream branches" that are named the same as the module streams.

```yaml
$ mkdir -p ~/flasklab/flask
$ cd ~/flasklab/flask
$ git init
$ git commit --allow-empty -m "Initial creation of repository for flask module"
$ git checkout -b py27 master
$ cat <<EOF > flask.yaml
---
document: modulemd
version: 2
data:
  name: flask
  stream: py27
  summary: Flask demo
  description: >-
    Flask demo. Python 2 version.
  license:
    module:
    - MIT
  dependencies:
  - buildrequires:
      platform: [el8]
      python27: [2.7]
    requires:
      platform: [el8]
      python27: [2.7]
  profiles:
    default:
      description: Default installation
      rpms:
      - python2-flask
      - python2-flask-sqlalchemy
  api:
    rpms:
    - python-flask
    - python-flask-sqlalchemy
  filter:
    rpms:
    - python3-click
    - python3-itsdangerous
    - python3-werkzeug
    - python3-flask
    - python3-flask-sqlalchemy
  buildopts:
    rpms:
      macros: |
        %_with_python2 1
  components:
    rpms:
      python-click:
        rationale: Dependency
        ref: el8
        repository: file:///root/rpms/python-click
        buildorder: 10
      python-itsdangerous:
        rationale: Dependency
        ref: el8
        repository: file:///root/rpms/python-itsdangerous
        buildorder: 10
      python-werkzeug:
        rationale: Dependency
        ref: el8
        repository: file:///root/rpms/python-werkzeug
        buildorder: 10
      python-flask:
        rationale: Package in api
        ref: el8
        repository: file:///root/rpms/python-flask
        buildorder: 20
      python-flask-sqlalchemy:
        rationale: Package in api
        ref: el8
        repository: file:///root/rpms/python-flask-sqlalchemy
        buildorder: 30
...
EOF
$ git add flask.yaml
$ git commit -m "Initial commit of flask:py27"
$ git checkout -b py36 master
$ cat <<EOF > flask.yaml
---
document: modulemd
version: 2
data:
  name: flask
  stream: py36
  summary: Flask demo
  description: >-
    Flask demo. Python 3 version.
  license:
    module:
    - MIT
  dependencies:
  - buildrequires:
      platform: [el8]
      python36: [3.6]
    requires:
      platform: [el8]
      python36: [3.6]
  profiles:
    default:
      description: Default installation
      rpms:
      - python3-flask
      - python3-flask-sqlalchemy
  api:
    rpms:
    - python-flask
    - python-flask-sqlalchemy
  components:
    rpms:
      python-flask:
        rationale: Package in api
        ref: el8
        repository: file:///root/rpms/python-flask
        buildorder: 10
      python-flask-sqlalchemy:
        rationale: Package in api
        ref: el8
        repository: file:///root/rpms/python-flask-sqlalchemy
        buildorder: 20
...
EOF
$ git add flask.yaml
$ git commit -m "Initial commit of flask:py36"
```
The Python 3 version of the modulemd file is much simpler because RHEL8 already includes the Python 3 dependencies.

Before we can build the modules, however, we need to make some corrections to the `python-flask` package's SPEC file
so the Python 2 version will build properly for RHEL8--and only when Python 2 packages are specifically requested.
We also need to disable the `%check` section since it will fail due to limitations of our containerized MBS--and to speed up the build.
The corrections need to be committed to `git`, so we'll put it in a new `git` branch called `el8`.
We will base these changes off of the master branch as of the time of the lab's creation.
To ensure a consistent experience, the command references the exact git hash to avoid changes in Fedora resulting in issues with the lab.

```bash
$ mkdir -p ~/flasklab/rpms
$ cd ~/flasklab/rpms
$ fedpkg clone -a python-flask
$ cd python-flask
$ git checkout -b el8 bc4932d
$ vi python-flask.spec
...
$ git diff
```
```diff
diff --git a/python-flask.spec b/python-flask.spec
index 49ad6b9..da70291 100644
--- a/python-flask.spec
+++ b/python-flask.spec
@@ -1,3 +1,10 @@
+%if 0%{?rhel} >= 8
+# Disable python2 build by default
+%bcond_with python2
+%else
+%bcond_without python2
+%endif
+
 %global modname flask
 %global srcname Flask
 
@@ -25,13 +32,14 @@ authentication technologies and more.
 
 %description %{_description}
 
+%if %{with python2}
 %package -n python2-%{modname}
 Summary:        %{summary}
 %{?python_provide:%python_provide python2-%{modname}}
 BuildRequires:  python2-devel
 BuildRequires:  python2-setuptools
 BuildRequires:  python2-pytest
-%if 0%{?fedora} >= 26
+%if 0%{?fedora} >= 26 || 0%{?rhel} >= 8
 BuildRequires:  python2-werkzeug
 Requires:       python2-werkzeug
 BuildRequires:  python2-jinja2
@@ -53,6 +61,7 @@ Requires:       python-itsdangerous
 %description -n python2-%{modname} %{_description}
 
 Python 2 version.
+%endif # with python2
 
 %package -n python%{python3_pkgversion}-%{modname}
 Summary:        %{summary}
@@ -86,25 +95,37 @@ rm -rf examples/flaskr/
 rm -rf examples/minitwit/
 
 %build
+%if %{with python2}
 %py2_build
+%endif # with python2
 %py3_build
 
 %install
+%if %{with python2}
 %py2_install
 mv %{buildroot}%{_bindir}/%{modname}{,-%{python2_version}}
 ln -s %{modname}-%{python2_version} %{buildroot}%{_bindir}/%{modname}-2
+%endif # with python2
 
 %py3_install
 mv %{buildroot}%{_bindir}/%{modname}{,-%{python3_version}}
 ln -s %{modname}-%{python3_version} %{buildroot}%{_bindir}/%{modname}-3
 
+%if %{with python2}
 ln -sf %{modname}-2 %{buildroot}%{_bindir}/%{modname}
+%else
+ln -sf %{modname}-3 %{buildroot}%{_bindir}/%{modname}
+%endif # with python2
 
 %check
+exit 0
 export LC_ALL=C.UTF-8
+%if %{with python2}
 PYTHONPATH=%{buildroot}%{python2_sitelib} py.test-%{python2_version} -v
+%endif # with python2
 PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-%{python3_version} -v || :
 
+%if %{with python2}
 %files -n python2-%{modname}
 %license LICENSE
 %doc CHANGES.rst README.rst
@@ -114,6 +135,7 @@ PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-%{python3_version} -v || :
 %{python2_sitelib}/%{modname}/
 
 %{_bindir}/%{modname}
+%endif # with python2
 
 %files -n python%{python3_pkgversion}-%{modname}
 %license LICENSE
@@ -123,6 +145,10 @@ PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-%{python3_version} -v || :
 %{python3_sitelib}/%{srcname}-*.egg-info/
 %{python3_sitelib}/%{modname}/
 
+%if %{without python2}
+%{_bindir}/%{modname}
+%endif # without python2
+
 %files doc
 %license LICENSE
 %doc examples
```
```bash
$ git commit -m "Updates for RHEL8" python-flask.spec
```

Note: If you're unsure of the precision of your SPEC file modifications above, or just want to avoid the tediousness of making the changes,
you can copy a pre-modified version from the lab support files:

```bash
$ cp ~/appstream-lab/labs/lab2/support-files/python-flask.spec.el8 ~/flasklab/rpms/python-flask/python-flask.spec
```

We are also including the `python-flask-sqlalchemy` package in our module since it includes some additional functionality we'll need.
The SPEC file needs one small correction so the Python 2 version will build properly for RHEL8, and several more corrections so the Python 2 packages only build when wanted.
And again, the updates need to be committed to `git` — which we'll put in a new `git` branch called `el8`.
We will base these changes off of the master branch as of the time of the lab's creation.
To ensure a consistent experience, the command references the exact git hash to avoid changes in Fedora resulting in issues with the lab.
```bash
$ cd ~/flasklab/rpms
$ fedpkg clone -a python-flask-sqlalchemy
$ cd python-flask-sqlalchemy
$ git checkout -b el8 55641d7
$ vi python-flask-sqlalchemy.spec
...
$ git diff
```
```diff
diff --git a/python-flask-sqlalchemy.spec b/python-flask-sqlalchemy.spec
index c6e33cb..131e475 100644
--- a/python-flask-sqlalchemy.spec
+++ b/python-flask-sqlalchemy.spec
@@ -1,9 +1,16 @@
 %global mod_name Flask-SQLAlchemy
-%if 0%{?fedora}
+%if 0%{?fedora} || 0%{?rhel} >= 8
 # there's no python3 in el*, disabling the python3 build
 %global with_python3 1
 %endif
 
+%if 0%{?rhel} >= 8
+# Disable python2 build by default
+%bcond_with python2
+%else
+%bcond_without python2
+%endif
+
 Name:           python-flask-sqlalchemy
 Version:        2.3.2
 Release:        4%{?dist}
@@ -14,7 +21,9 @@ URL:            https://github.com/mitsuhiko/flask-sqlalchemy
 Source0:        https://pypi.io/packages/source/F/%{mod_name}/%{mod_name}-%{version}.tar.gz
 
 BuildArch:      noarch
+%if %{with python2}
 BuildRequires:  python2-devel
+%endif
 
 %description
 Flask-SQLAlchemy is an extension for Flask that adds support for
@@ -22,12 +31,13 @@ SQLAlchemy to your application. It aims to simplify using SQLAlchemy with
 Flask by providing useful defaults and extra helpers that make it easier
 to accomplish common tasks.
 
+%if %{with python2}
 %package -n python2-flask-sqlalchemy
 Summary:        Adds SQLAlchemy support to Flask application
 %{?python_provide:%python_provide python2-%{mod_name}}
 %{?python_provide:%python_provide python2-flask-sqlalchemy}
 BuildRequires:  python2-devel
-%if 0%{?fedora}
+%if 0%{?fedora} || 0%{?rhel} >= 8
 BuildRequires:  python2-setuptools
 BuildRequires:  python2-pytest
 BuildRequires:  python2-flask
@@ -50,6 +60,7 @@ Flask by providing useful defaults and extra helpers that make it easier
 to accomplish common tasks.
 
 Python 2 version.
+%endif # with python2
 
 %if 0%{?with_python3}
 %package -n python3-flask-sqlalchemy
@@ -80,36 +91,44 @@ rm -f docs/.DS_Store
 rm -f docs/_themes/.gitignore
 
 %build
+%if %{with python2}
 %py2_build
+%endif
 
 %if 0%{?with_python3}
 %py3_build
 %endif
 
 %install
+%if %{with python2}
 %py2_install
+%endif
 
 %if 0%{?with_python3}
 %py3_install
 %endif
 
 %check
+%if %{with python2}
 %if 0%{?fedora}
 PYTHONPATH=%{buildroot}/%{python2_sitelib} py.test-2
 %else
 # pytest is too old on el7..
 #PYTHONPATH=%%{buildroot}/%%{python2_sitelib} py.test
 %endif
+%endif # with python2
 
 %if 0%{?with_python3}
 PYTHONPATH=%{buildroot}/%{python3_sitelib} py.test-3
 %endif
 
+%if %{with python2}
 %files -n python2-flask-sqlalchemy
 %license LICENSE
 %doc docs/ README CHANGES.rst PKG-INFO
 %{python2_sitelib}/*.egg-info/
 %{python2_sitelib}/flask_sqlalchemy/
+%endif # with python2
 
 %if 0%{?with_python3}
 %files -n python3-flask-sqlalchemy
```
```bash
$ git commit -m "Updates for RHEL8" python-flask-sqlalchemy.spec
```

Note: You can copy a pre-modified version of the SPEC file from the lab support files:

```bash
$ cp ~/appstream-lab/labs/lab2/support-files/python-flask-sqlalchemy.spec.el8 ~/flasklab/rpms/python-flask-sqlalchemy/python-flask-sqlalchemy.spec
```

Furthermore, since RHEL8 doesn't provide all Python 2 packages,
we need to set things up to build the Python 2 versions of a few dependent packages.
Those dependent packages were already included in the modulemd file above.

For dependency `python-werkzeug`, let's roll back to a version that happens to be the same version used for RHEL8's Python 3 package.
Let's also disable the documentation subpackages for this exercise to avoid getting into a long chain of additional dependencies.

And, as before, the updates need to be committed to `git` — which again we'll put in a new `git` branch called `el8`.

We will base these changes off of the f28 branch as of the time of the lab's creation.
To ensure a consistent experience, the command references the exact git hash to avoid changes in Fedora resulting in issues with the lab.

```bash
$ cd ~/flasklab/rpms
$ fedpkg clone -a python-werkzeug
$ cd python-werkzeug
$ git checkout -b el8 95a06aa
$ vi python-werkzeug.spec
...
$ git diff
```
```diff
diff --git a/python-werkzeug.spec b/python-werkzeug.spec
index 3ded80c..ddc1ce3 100644
--- a/python-werkzeug.spec
+++ b/python-werkzeug.spec
@@ -46,6 +46,7 @@ BuildRequires:  python2-setuptools
 
 %description -n python2-werkzeug %_description
 
+%if 0
 %package -n python2-werkzeug-doc
 Summary:        Documentation for %{name}
 
@@ -56,6 +57,7 @@ Requires:       python2-werkzeug = %{version}-%{release}
 
 %description -n python2-werkzeug-doc
 Documentation and examples for %{name}.
+%endif
 
 
 %package -n python3-werkzeug
@@ -69,6 +71,7 @@ BuildRequires:  python3-setuptools
 %description -n python3-werkzeug %_description
 
 
+%if 0
 %package -n python3-werkzeug-doc
 Summary:        Documentation for python3-werkzeug
 
@@ -79,6 +82,7 @@ Requires:       python3-werkzeug = %{version}-%{release}
 
 %description -n python3-werkzeug-doc
 Documentation and examples for python3-werkzeug.
+%endif
 
 
 %prep
@@ -96,23 +100,27 @@ find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
 %py2_build
 find examples/ -name '*.py' -executable | xargs chmod -x
 find examples/ -name '*.png' -executable | xargs chmod -x
+%if 0
 pushd docs
 # Add a symlink to the dir with the Python module so that __version__ can be
 # obtained therefrom.
 ln -s ../werkzeug werkzeug
 make html
 popd
+%endif
 
 pushd %{py3dir}
 %py3_build
 find examples/ -name '*.py' -executable | xargs chmod -x
 find examples/ -name '*.png' -executable | xargs chmod -x
+%if 0
 pushd docs
 # Add a symlink to the dir with the Python module so that __version__ can be
 # obtained therefrom.
 ln -s ../werkzeug werkzeug
 make html
 popd
+%endif
 popd
 
 
@@ -132,16 +140,20 @@ popd
 %doc AUTHORS PKG-INFO CHANGES
 %{python2_sitelib}/*
 
+%if 0
 %files -n python2-werkzeug-doc
 %doc docs/_build/html examples
+%endif
 
 %files -n python3-werkzeug
 %license LICENSE
 %doc AUTHORS PKG-INFO CHANGES
 %{python3_sitelib}/*
 
+%if 0
 %files -n python3-werkzeug-doc
 %doc docs/_build/html examples
+%endif
 
 
 %changelog
```
```bash
$ git commit -m "Updates for RHEL8" python-werkzeug.spec
```

Note: You can copy a pre-modified version of the SPEC file from the lab support files:

```bash
$ cp ~/appstream-lab/labs/lab2/support-files/python-werkzeug.spec.el8 ~/flasklab/rpms/python-werkzeug/python-werkzeug.spec
```

The `python-click` and `python-itsdangerous` dependencies also need to be made available for inclusion in the module.
Fortunately, no changes are needed to the SPEC file, but we'll create an `el8` branch anyhow.

We will base these changes off of the master branch as of the time of the lab's creation.
To ensure a consistent experience, the command references the exact git hash to avoid changes in Fedora resulting in issues with the lab.

```bash
$ cd ~/flasklab/rpms
$ fedpkg clone -a python-click
$ cd python-click
$ git checkout -b el8 cf7c963
```

```bash
$ cd ~/flasklab/rpms
$ fedpkg clone -a python-itsdangerous
$ cd python-itsdangerous
$ git checkout -b el8 b73ffe1
$ cd ~/flasklab/rpms
```

## Build out modules

Eventually, we expect that `module-build-service` (or `MBS` for short) will be available as an RPM for RHEL8, for now we are using a prebuilt container that's based on Fedora.
To get it:

```bash
$ cd ~/appstream-lab/mbs-container
$ sudo make pull
```

Make container wrapper available from the command line:

```bash
$ ln -sf $PWD/mbs-manager.sh ~/bin/mbs-manager
```

Now we will actually build the modules.

Build the py27 stream
```bash
$ mkdir -p ~/flasklab/repos
$ cd ~/flasklab/flask
$ git checkout py27
$ cd ~/flasklab/
$ sudo /home/student/bin/mbs-manager build_module_locally --offline --file flask/flask.yaml --stream py27 -s platform:el8 -p platform:el8 -r /etc/yum.repos.d/rhel8.repo | tee build27.log
$ sudo mv modulebuild/builds/$(ls -t1 modulebuild/builds |head -n1)/results /home/student/flasklab/repos/py27
```
If this step fails, examine the logs output into `build27.log` and `~/flasklab/modulebuild/builds/module-flask-py27-<something>/results/` for errors.
When correcting errors in the spec files, you **must** commit your changes to git or the MBS will not incorporate them.

### Examine the MBS generated module repo
```bash
$ yum --repofrompath=flaskmod,/home/student/flasklab/repos/py27 --repo=flaskmod module list
$ yum --repofrompath=flaskmod,/home/student/flasklab/repos/py27 --repo=flaskmod module info flask
```


Build the py36 stream
```bash
$ mkdir -p ~/flasklab/repos
$ cd ~/flasklab/flask
$ git checkout py36
$ cd ~/flasklab/
$ sudo /home/student/bin/mbs-manager build_module_locally --offline --file flask/flask.yaml --stream py36 -s platform:el8 -p platform:el8 -r /etc/yum.repos.d/rhel8.repo 2>&1 | tee build36.log
$ sudo mv modulebuild/builds/$(ls -t1 modulebuild/builds |head -n1)/results /home/student/flasklab/repos/py36
```
If this step fails, examine the logs output into `build36.log` and `~/flasklab/modulebuild/builds/module-flask-py36-<something>/results/` for errors.
When correcting errors in the spec files, you **must** commit your changes to git or the MBS will not incorporate them.

### Examine the MBS generated module repo
```bash
$ yum --repofrompath=flaskmod,/home/student/flasklab/repos/py36 --repo=flaskmod module list
$ yum --repofrompath=flaskmod,/home/student/flasklab/repos/py36 --repo=flaskmod module info flask
```

Module Build Service is integrated in to `fedpkg`, however the `--offline` flag is new in `mbs` and has not been integrated yet.
Soon, you should be able to do something like the following:

```bash
$ fedpkg module-build-local --offline --file flask.yaml --stream py27 -r /etc/yum.repos.d/rhel8.repo
$ fedpkg module-build-local --offline --file flask.yaml --stream py36 -r /etc/yum.repos.d/rhel8.repo
```


## Create a repo file
```bash
$ cat <<EOF | sudo tee /etc/yum.repos.d/local.repo
[python36-flask]
name=python36 flask module
baseurl=file:///home/student/flasklab/repos/py36
enabled=1
repo_gpgcheck=0
gpgcheck=0
skip_if_unavailable=True

[python27-flask]
name=python27 flask module
baseurl=file:///home/student/flasklab/repos/py27
enabled=1
repo_gpgcheck=0
gpgcheck=0
skip_if_unavailable=True
EOF
$ yum module list
```
You should now see your two new modules listed (towards the top).

These repos could also be hosted on a webserver simply by placing the directories in an area served via HTTP(S) and updating the `baseurl=...` lines in the local repo file with the URLs to those directories.

Now we can move on to [Lab 3](../lab3/chapter3.md) where we will actually use the new modules.
