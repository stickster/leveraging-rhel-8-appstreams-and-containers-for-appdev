[//]: # (Comment: This document uses the practice of breaking all sentences in to new lines to ease diff comparisons.)
# Lab 4: Moving to OpenShift
## Introduction

In this lab we will migrate our application to a git forge and then deploy the application to OpenShift with webhooks.

#TODO server and username ->
This lab should be performed on #TODO as #TODO unless otherwise instructed.

Expected completion: 15-20 minutes #TODO fix times

NOTE: much of the superflous output from the commands run here has been redacted for brevity. Please don't consider it an error.

## General Information

* url for OpenShift server
* url for gitforge

## Push code to gitforge

*  create a repo
*  hook it up to the code on disk
*  push to repo
*  **NOTE: not planning on automatically rebuilding the module, just redeploying python code

## Create OpenShift Project

*  create the project
*  add database from container catalog
*  configure front-end to source from gitforge
*  make the flask modules available
*  2 projects or 1?

## Deploy

* deploy the app
* verify it works
